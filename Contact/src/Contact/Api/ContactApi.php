<?php 
namespace Contact\Api;
use Core\Functions;

use Common\Api\Api;

class  ContactApi extends Api{
	protected static $_entity = '\Contact\Entity\Contact';
	public static function getContactById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
}
