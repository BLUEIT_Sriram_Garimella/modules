<?php
namespace Contact\Form\ContactForm;

use Common\Form\Option\AbstractFormFactory;

class ContactFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Contact\Form\ContactForm\ContactForm' );
		$form->setName ( 'contactForm' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Contact\Form\ContactForm\ContactFormFilter' );
	}
}
