<?php
namespace Contact\Form\AdminContactConfigAdmin;

use Common\Form\Option\AbstractFormFactory;

class ConfigAdminFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Contact\Form\AdminContactConfigAdmin\ConfigAdminForm' );
		$form->setName ( 'adminContactConfigAdmin' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Contact\Form\AdminContactConfigAdmin\ConfigAdminFilter' );
	}
}
