<?php
namespace Contact\Form\ContactForm;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\Regex;


class ContactFormFilter extends InputFilter

{

    protected $inputFilter;

    public function __construct()
    {
        $this->add ( array (
        		'name' => 'name',
        		'required' => true,
        		'filters' => array (
        				array (
        						'name' => 'StringTrim'
        				),
        				array (
        						'name' => 'StripTags'
        				)
        		),
        		'validators' => array (
        				array (
        						'name' => 'StringLength',
        						'options' => array (
        								'encoding' => 'UTF-8',
        								'min' => 1,
        								'max' => 255
        						)
        				),
        				array (
        						'name' => 'regex',
        						'options' => array (
        								'pattern' => "/^[a-zA-Z ,.'-]+$/",
        								'messages' => array (
        										Regex::NOT_MATCH => 'Use Letters & periods'
        								)
        						)
        				)
        		)
        ) );
        
        $this->add ( array (
        		'name' => 'email',
        		'required' => true,
        		'filters' => array (
        				array (
        						'name' => 'StringTrim'
        				)
        		),
        		'validators' => array (
        				array (
        						'name' => 'EmailAddress'
        				)
        		)
        ) );
        
        
    
        
        $this->add ( array (
        		'name' => 'contactNumber',
        		'required' => false,
        		'filters' => array (
        				array (
        						'name' => 'StringTrim'
        				)
        		),
        		'validators' => array (
        				array (
        						'name' => 'regex',
        						'options' => array (
        								'pattern' => '/^(?!.*-.*-.*-)(?=(?:\d{8,13}$)|(?:(?=.{9,13}$)[^-]*-[^-]*$)|(?:(?=.{10,13}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/',
        								'messages' => array (
        										Regex::NOT_MATCH => 'Invalid Phone number'
        								)
        						)
        				)
        		)
        ) );
        
        
        $this->add(array(
            'name' => 'message',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));
       
    }
} 