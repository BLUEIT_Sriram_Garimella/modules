<?php
namespace Contact\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Api\ConfigApi;

class ContactMail extends AbstractMainFormEvent
{

    public function getFormName()
    {
        return 'contactForm';
    }

    public function getPriority()
    {
        return -100;
    }

    public function save()
    {
        $form = $this->getForm();
        $params = $form->getData();
        
        $resultContainer = $this->getFormResultContainer();
        $contact = $resultContainer->get('contact')->getValue();
        
        if ($contact) {
            
            $this->sendEmail($contact);
        }
        return true;
    }

    /**
     *
     * @return \Common\Option\Token\TokenContainer
     */
    public function sendEmail($contact)
    {
        $subject = ConfigApi::getConfigByKey('CONTACT_USER_SUBJECT');
        $bodyHtml = ConfigApi::getConfigByKey('CONTACT_USER_BODYHTML');
        $bodyPlain = ConfigApi::getConfigByKey('CONTACT_USER_BODYPLAIN');
        
        $fromName = ConfigApi::getConfigByKey('CONTACT_FROM_NAME', '');
        $fromEmail = ConfigApi::getConfigByKey('CONTACT_FROM_EMAIL');
        
        if (! $fromEmail) {
            return;
        }
        
        if ($subject && ($bodyHtml || $bodyPlain)) {
            
            $tokenContainer = $this->getTokenContainer();
            $tokenContainer->add('contact', $contact);
            $tokenContainer->addParam('contactId', $contact->id);
            $tokenContainer->setSubject($subject);
            $tokenContainer->setBodyHtml($bodyHtml);
            $tokenContainer->setBodyText($bodyPlain);
            $tokenContainer->setFromEmail($fromEmail);
            $tokenContainer->setFromName($fromName);
            
            $tokenContainer->setToEmail($contact->email);
            $tokenContainer->setToName($contact->name);
            
            $tokenContainer->prepare('contactFormMail');
            
            $tokenContainer->sendMail();
        }
        
        $subject = ConfigApi::getConfigByKey('CONTACT_ADMIN_SUBJECT');
        $bodyHtml = ConfigApi::getConfigByKey('CONTACT_ADMIN_BODYHTML');
        $bodyPlain = ConfigApi::getConfigByKey('CONTACT_ADMIN_BODYPLAIN');
        $notify = ConfigApi::getConfigByKey('CONTACT_ADMIN_NOTIFY');
        $toName = ConfigApi::getConfigByKey('CONTACT_ADMIN_FROM_NAME');
        $toEmail = ConfigApi::getConfigByKey('CONTACT_ADMIN_FROM_EMAIL');
        
        if ($subject && ($bodyHtml || $bodyPlain) && $toEmail && $notify) {
            
            $tokenContainer = $this->getTokenContainer();
            $tokenContainer->add('contact', $contact);
            $tokenContainer->addParam('contactId', $contact->id);
            $tokenContainer->setSubject($subject);
            $tokenContainer->setBodyHtml($bodyHtml);
            $tokenContainer->setBodyText($bodyPlain);
            $tokenContainer->setFromEmail($fromEmail);
            $tokenContainer->setFromName($fromName);
            
            $tokenContainer->setToEmail($toEmail);
            $tokenContainer->setToName($toName);
            
            $tokenContainer->prepare('contactFormMail');
            $tokenContainer->sendMail();
        }
        
        return $tokenContainer;
    }

    /**
     *
     * @return TokenContainer
     */
    public function getTokenContainer()
    {
        return $this->getServiceLocator()->get('TokenContainer');
    }
}
