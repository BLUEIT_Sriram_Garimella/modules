<?php 
namespace Contact\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminContactConfig extends AbstractTabEvent {
	public function getEventName() {
		return 'adminContactConfig';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-contact-config-general');
		$tabContainer->add ( 'admin-contact-config-general', 'General', $u,1000 );
		
		$u = $url ( 'admin-contact-config-user');
		$tabContainer->add ( 'admin-contact-config-user', 'User', $u,900 );
		
		$u = $url ( 'admin-contact-config-admin');
		$tabContainer->add ( 'admin-contact-config-admin', 'Admin', $u,800 );
		
		return $this;
	}
}

