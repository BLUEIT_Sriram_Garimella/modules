<?php
namespace Contact\Form\AdminContactConfigAdmin;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class ConfigAdminForm extends Form 

{
	public function init() {
		
	    $this->add ( array (
	    		'name' => 'CONTACT_ADMIN_FROM_NAME',
	    		'attributes' => array (
	    				'type' => 'text'
	    		),
	    		'options' => array (
	    				'label' => 'Notification From Name'
	    		)
	    ), array (
	    		'priority' => 1000
	    ) );
	    
	    $this->add ( array (
	    		'name' => 'CONTACT_ADMIN_FROM_EMAIL',
	    		'attributes' => array (
	    				'type' => 'text'
	    		),
	    		'options' => array (
	    				'label' => 'Notification From Email'
	    		)
	    ), array (
	    		'priority' => 990
	    ) );
	    
	    
	    $this->add ( array (
	    		'type' => 'Zend\Form\Element\Checkbox',
	    		'name' => 'CONTACT_ADMIN_NOTIFY',
	    		'options' => array (
	    				'label' => 'Notify Admin',
	    				'use_hidden_element' => true
	    		)
	    ), array (
	    		'priority' => 980
	    ) );
	    
	   
	    
	    $this->add ( array (
				'name' => 'CONTACT_ADMIN_SUBJECT',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Subject' 
				) 
		), array (
				'priority' => 970 
		) );
		
		$this->add ( array (
				'name' => 'CONTACT_ADMIN_BODYHTML',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full'
						 
				),
				'options' => array (
						'label' => 'HTML Message' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'CONTACT_ADMIN_BODYPLAIN',
				'attributes' => array (
						'type' => 'textarea' 
				)
				,
				'options' => array (
						'label' => 'Plain Message' 
				) 
		), array (
				'priority' => 950 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}