<?php 
namespace Contact\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminContactConfigGeneral extends AdminConfig {
    protected $_columnKeys = array (
    		'CONTACT_FROM_EMAIL',
    		'CONTACT_FROM_NAME'		
    );
	public function getFormName() {
		return 'adminContactConfigGeneral';
	}
	public function getPriority() {
		return 1000;
	}
	
}
