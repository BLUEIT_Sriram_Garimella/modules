<?php 
namespace Contact\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminContactConfigUser extends AdminConfig {
	 protected $_columnKeys = array (
    		'CONTACT_USER_SUBJECT',
    		'CONTACT_USER_BODYHTML',
	        'CONTACT_USER_BODYPLAIN'
    );
	
	public function getFormName() {
		return 'adminContactConfigUser';
	}
	public function getPriority() {
		return 1000;
	}
	
}
