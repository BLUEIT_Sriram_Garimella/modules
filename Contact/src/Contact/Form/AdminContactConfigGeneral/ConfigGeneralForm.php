<?php
namespace Contact\Form\AdminContactConfigGeneral;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class ConfigGeneralForm extends Form 

{
	public function init() {
	    $this->add ( array (
	    		'name' => 'CONTACT_FROM_NAME',
	    		'attributes' => array (
	    				'type' => 'text'
	    		),
	    		'options' => array (
	    				'label' => 'From Name'
	    		)
	    ), array (
	    		'priority' => 1000
	    ) );
	    
	    $this->add ( array (
	    		'name' => 'CONTACT_FROM_EMAIL',
	    		'attributes' => array (
	    				'type' => 'text'
	    		),
	    		'options' => array (
	    				'label' => 'From Email'
	    		)
	    ), array (
	    		'priority' => 990
	    ) );
	    
	       $this->add ( array (
	    		'name' => 'btnsubmit',
	    		'attributes' => array (
	    				'type' => 'submit',
	    				'value' => 'Submit'
	    		)
	    ), array (
	    		'priority' => - 100
	    ) );
		
		
	}
	
}