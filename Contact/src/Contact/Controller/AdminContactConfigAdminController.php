<?php

namespace Contact\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminContactConfigAdminController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Contact\Form\AdminContactConfigAdmin\ConfigAdminFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-contact-config-admin' );
	}
}