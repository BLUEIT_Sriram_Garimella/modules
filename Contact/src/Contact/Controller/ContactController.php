<?php

namespace Contact\Controller;

use Common\MVC\Controller\AbstractAdminController;
use Common\MVC\Controller\AbstractFrontController;
use Zend\View\Model\ViewModel;
use Core\Functions;

class ContactController extends AbstractFrontController {
public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Contact\Form\ContactForm\ContactFormFactory' );
	}
	public function afterSave() {
		$form = $this->getFormFactory ();
	}
	public function getSaveRedirector() {
		return $this->redirect ()->toRoute ( 'contact' );
	}
	public function contactAction() {
		$form = $this->getFormFactory ();
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				$this->afterSave ();
				$this->addSuccessMessage ();
				return $this->getSaveRedirector ();
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
	public function addSuccessMessage() {
		Functions::addSuccessMessage ( 'Thank you for contacting us.' );
	}
	
	
}