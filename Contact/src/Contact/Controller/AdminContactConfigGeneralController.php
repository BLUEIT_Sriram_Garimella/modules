<?php

namespace Contact\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminContactConfigGeneralController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Contact\Form\AdminContactConfigGeneral\ConfigGeneralFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-contact-config-general' );
	}
}