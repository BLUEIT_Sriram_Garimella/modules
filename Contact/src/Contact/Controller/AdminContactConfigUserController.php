<?php

namespace Contact\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminContactConfigUserController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Contact\Form\AdminContactConfigUser\ConfigUserFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-contact-config-user' );
	}
}