<?php

namespace Contact\Service\Navigation;

use Common\Navigation\Event\NavigationEvent;

class ContactNavigation extends NavigationEvent {

    public function breadcrumb() {
        parent::breadcrumb();
        $page = array(
            'label' => 'Contact Us',
            'route' => 'contact',
            'id' => 'contact'
        );

        $this->addDefaultMenu($page);
    }

}
